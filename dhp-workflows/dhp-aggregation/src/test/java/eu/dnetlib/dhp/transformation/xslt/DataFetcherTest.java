package eu.dnetlib.dhp.transformation.xslt;

import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DataFetcherTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getJson() throws IOException, URISyntaxException {
        URL contributorsUrl = new URI("https://api.osf.io/v2/preprints/mrwqb/contributors/?format=json").toURL();
        JSONObject testJsonObj = DataFetcher.getJson(contributorsUrl);

        String x = testJsonObj
                .getJSONArray("data")
                .getJSONObject(0)
                .getJSONObject("embeds")
                .getJSONObject("users")
                .getJSONObject("data")
                .getJSONObject("attributes")
                .getString("full_name");
        System.out.println(x);
        System.out.println(testJsonObj.getJSONArray("data").length());
        testJsonObj.getJSONArray("data").forEach(System.out::println);
    }

    @Test
    void getAuthorsFromJson() throws IOException, URISyntaxException {
        URL contributorsUrl = new URI("https://api.osf.io/v2/preprints/mrwqb/contributors/?format=json").toURL();
        JSONObject testJsonObj = DataFetcher.getJson(contributorsUrl);
        List<String> authors = DataFetcher.getAuthorsFromJson(testJsonObj);
        System.out.println(authors);
        System.out.println(DataFetcher.transformListToDublinCore(authors));
    }

    @Test
    void getAndTransformAuthors() throws IOException, URISyntaxException {
        URL contributorsUrl = new URI("https://api.osf.io/v2/preprints/mrwqb/contributors/?format=json").toURL();
        System.out.println(DataFetcher.getAndTransformAuthors(contributorsUrl));
    }


    @Test
    void getLinkToFulltextFromJson() throws URISyntaxException, IOException {
        URL linkToFullTextDocument = new URI("https://api.osf.io/v2/files/5de7c96f84c479000c7928af/?format=json").toURL();
        System.out.println(DataFetcher.getFullTextLinkAndTransform(linkToFullTextDocument));


    }
}