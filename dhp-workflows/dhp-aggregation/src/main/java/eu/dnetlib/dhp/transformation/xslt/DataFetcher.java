package eu.dnetlib.dhp.transformation.xslt;

import java.io.Serializable;
import net.sf.saxon.s9api.*;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * This class fetches JSON from a provided link and returns
 * a Dublin Core. This functionality is particularly needed for OSF Preprints
 */


public class DataFetcher implements ExtensionFunction, Serializable {

    /**
     * This method fetches JSON object from a given URL
     * @param url a url in the metadata for fetching authors in JSON format
     * @return
     * @throws IOException
     */
    static JSONObject getJson(URL url) throws IOException {

        String json = IOUtils.toString(url);
        return new JSONObject(json);
    }

    /**
     * This method extracts authors from a given JSON
     *
     * @param jsonObject
     * @return
     */
    static List<String> getAuthorsFromJson(JSONObject jsonObject) {
        List<String> authors = new ArrayList<>();
        // count of authors
        int countOfAuthors = jsonObject.getJSONArray("data").length();
        for (int i = 0; i < countOfAuthors; i++) {

            authors.add(jsonObject
                    .getJSONArray("data")
                    .getJSONObject(i)
                    .getJSONObject("embeds")
                    .getJSONObject("users")
                    .getJSONObject("data")
                    .getJSONObject("attributes")
                    .getString("full_name"));
        }
        return authors;
    }

    /**
     * This method transforms list of authors into Dublin Core
     * @param authors
     * @return Dublin Core list of authors
     */
    static List<String> transformListToDublinCore(List<String> authors) {

        List<String> dublinCoreAuthors = new ArrayList<>();
        for (String author : authors){

            //splitting full name into first and last names according to OpenAIRE v3 guidelines at:
            // https://guidelines.openaire.eu/en/latest/literature/field_creator.html
            // “surname”, “initials” (“first name”) “prefix”.
            String[] parts = author.split(" ");
            String firstName = parts[0];
            String lastName = parts[1];
            char initialOfFirstName = firstName.charAt(0);

            dublinCoreAuthors.add(
                    "<dc:creator>" + lastName + ", " + initialOfFirstName +  ". (" + firstName + ")" + "</dc:creator>");
        }
        return dublinCoreAuthors;
    }

     /**
     * This is a public method which fetches authors and transform them into Dublin Core
     */
    public static String getAndTransformAuthors(URL url) throws IOException{
        return String.join(", ", transformListToDublinCore(getAuthorsFromJson(getJson(url))));
    }


    /**
     * This method extracts link to fulltext from a given JSON
     *
     * @return
     */
    static private String getLinkToFulltextFromJson(JSONObject jsonObject) throws MalformedURLException {

        // note: Link to JSON containing fulltextlink is in "primary_file" attribute.
        // And in the resultant JSON,  “links->download” contains the URL to fulltext

        return jsonObject
                .getJSONObject("data")
                .getJSONObject("links")
                .getString("download");
    }

    /**
     * This is a public method which fetches link to full text and returns it as a suitable format
     */
    public static String getFullTextLinkAndTransform (URL url )throws IOException{

        return getLinkToFulltextFromJson(getJson(url));
    }


    @Override
    public QName getName() {
        return null;
    }

    @Override
    public SequenceType getResultType() {
        return null;
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[0];
    }

    @Override
    public XdmValue call(XdmValue[] xdmValues) throws SaxonApiException {
        return null;
    }
}
